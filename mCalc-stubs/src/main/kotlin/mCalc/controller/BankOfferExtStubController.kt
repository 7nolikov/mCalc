package mCalc.controller

import com.fasterxml.jackson.databind.ObjectMapper
import org.springframework.core.io.ClassPathResource
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController

@RestController
class BankOfferExtStubController {

    @GetMapping("/ext/banks/offers")
    fun getBankOffers(): Any? {
        val resource = ClassPathResource("/static/json/bankOffers.json")
        return ObjectMapper().readValue(resource.inputStream, Any::class.java)
    }
}