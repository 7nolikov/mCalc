package mCalc.controller

import com.fasterxml.jackson.databind.ObjectMapper
import org.springframework.core.io.ClassPathResource
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController

@RestController
class ApartmentOfferExtStubController {

    @GetMapping("/ext/apartments/offers")
    fun getApartmentOffers(): Any? {
        val resource = ClassPathResource("/static/json/apartmentOffers.json")
        return ObjectMapper().readValue(resource.inputStream, Any::class.java)
    }
}