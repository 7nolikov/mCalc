package mCalc.controller

import com.fasterxml.jackson.databind.ObjectMapper
import org.springframework.core.io.ClassPathResource
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController

@RestController
class InsuranceOfferExtStubController {

    @GetMapping("/ext/insurance/offers")
    fun getInsuranceOffers(): Any? {
        val resource = ClassPathResource("/static/json/insuranceOffers.json")
        return ObjectMapper().readValue(resource.inputStream, Any::class.java)
    }
}