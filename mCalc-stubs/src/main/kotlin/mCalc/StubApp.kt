package mCalc

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class StubApp

fun main(args: Array<String>) {
    runApplication<StubApp>(*args)
}