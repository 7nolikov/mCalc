FROM openjdk:8-jdk-alpine
RUN java -version
VOLUME /tmp
COPY mCalc-temp/target/*.jar app.jar
ENTRYPOINT ["java","-jar","/app.jar"]