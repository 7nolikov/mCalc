#!/usr/bin/env bash
function finish {
  trap - SIGTERM && kill 0
  echo All successfully killed!
}

trap finish INT TERM EXIT

echo Run docker!
if [[ ! "$(docker ps -q -f name=mcalc)" ]]; then
    if [[ "$(docker ps -aq -f status=exited -f name=mcalc)" ]]; then
        # cleanup
        docker rm mcalc
    fi
    # run your container
    docker run -d -p 5432:5432 --name mcalc -e POSTGRES_PASSWORD=root postgres
    docker ps
    echo Docker started!
fi

mvn clean install
echo Compile completed!

echo Run Stub!
find mCalc-stubs/target/ -name "*.jar" -exec java -jar {} \;

sleep 5
echo Stub started!
sleep 5

echo Run Application!
find mCalc-temp/target/ -name "*.jar" -exec java -jar {} \;