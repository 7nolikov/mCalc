# Mortgage calculator

[![Build Status](https://travis-ci.org/7nolikov/mCalc.svg?branch=master)](https://travis-ci.org/7nolikov/mCalc)[![Maintainability](https://api.codeclimate.com/v1/badges/b07abb5983ae101840d9/maintainability)](https://codeclimate.com/github/7nolikov/mCalc/maintainability)[![Test Coverage](https://api.codeclimate.com/v1/badges/b07abb5983ae101840d9/test_coverage)](https://codeclimate.com/github/7nolikov/mCalc/test_coverage)

## General requirements

1. Web application based on Kotlin and micro-service architecture
2. User has ability to compare mortgage option and purchase option basing on reports
3. User has ability to upload documents and check them for mortgage or purchase
4. User can register via email, vk, google or facebook
5. Bank aggregator service should received information about mortgage offers
and store them in single format (requirements, interest rate, etc.) 
6. Apartment aggregator service should received information about apartment purchase offers
and store them in single format (price, location, etc.)
7. Report service should generate report based on specified parameters:
     - mortgage calculator type: (Annuetent / Differential)
     - apartment price
     - apartment location
     - bank interest rate
     - mortgage period
8. Following parameters will be calculated and shown in report
    - First payment
    - Credit sum
    - Payment per month
    - Overpayment
    - Total payment
10. Mail service should provide user ability to subscribe for receiving best offers

## Use cases:

#### Guest process

1. User with Guest role navigates to any page of application
2. Redirect to /login page of SSO service
3. User has options to continue:
- Register
- Register via socials
- Login
- Reset password
 
#### Registration process

1. Starts on /login page
2. Click Register button
3. Enter registration parameters and submit form to `SsoService`
4. `MailService` sent verification email
5. `UserService` start Create user process

#### Registration via socials process

1. Starts on /login page
2. Click Register via socials button
3. Request to socials sent by `SsoService`
4. User should give access to social provider for application
5. `UserService` start Create user process

#### Login process

1. Starts on /login page
2. Click Login button
3. Enter login parameters and submit form to `SsoService`
4. User permissions granted by role fetched from `UserService`
5. Token provided

#### Reset password process

1. Starts on /login page
2. Click Reset password button

#### Logout process

1. User clicks Logout button
2. Token related to User is deleted

#### Create user process

1. `UserService` starts process by Registration process request
2. `UserService` save User entity with registration parameters in `DbService`
3. Default user role - User

#### Update user process

1. User clicks Update user button
2. User enter parameters and submit form to `UserService`
3. `UserService` update entity from `DbService`

#### Delete user process

1. User clicks Delete user button
2. User enter password and submit form to `UserService`
3. `UserService` Delete entity from `DbService`

#### Create intent process

1. User clicks on Create intent button
2. User enter intent parameters and submit form to `IntentService`
3. `IntentService` save entity in `DbService`

#### Update intent process

1. User clicks on Update intent button
2. User enter intent parameters and submit form to `IntentService`
3. `IntentService` update entity in `DbService`

#### Delete intent process

1. User clicks on Delete intent button
2. User enter intent parameters and submit form to `IntentService`
3. `IntentService` delete entity in `DbService`

#### Banks offers fetching process

1. Check bank offers providers availability
2. `BankService` fetch data

#### Apartment offers fetching process

1. Check apartment offers providers availability
2. `ApartmentService` fetch data

#### Insurance offers fetching process

1. Check insurance offers providers availability
2. `InsuranceService` fetch data

#### Notifications subscribe process
 
1. User clicks Subscribe button
2. `NotificationService` adding User to mailing list

#### Notifications unsubscribe process
 
1. User clicks Unsubscribe button
2. `NotificationService` deleting User from mailing list

#### Create report process

1. `ReportService` calculate report based on `Intent` from `DbService`

#### Best variant notifications process

1. `MailService` fetch best offers based on `Intent` from `DbService`
2. `MailService` send notifications

### Low priority features:

###### Documents uploading process

###### Documents validation process

###### Create billing account process

###### Update billing account process

###### Delete billing account process

###### Add tests 4