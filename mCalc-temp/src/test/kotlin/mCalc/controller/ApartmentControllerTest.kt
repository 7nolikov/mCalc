//package mCalc.controller
//
//import mCalc.dto.ApartmentOfferDto
//import org.amshove.kluent.`should equal`
//import org.amshove.kluent.`should not be`
//import org.junit.Test
//import org.junit.runner.RunWith
//import org.springframework.beans.factory.annotation.Autowired
//import org.springframework.boot.test.context.SpringBootTest
//import org.springframework.boot.test.web.client.TestRestTemplate
//import org.springframework.http.HttpStatus
//import org.springframework.test.context.junit4.SpringRunner
//import java.math.BigDecimal
//
//@RunWith(SpringRunner::class)
//@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
//class ApartmentControllerTest {
//
//    @Autowired
//    lateinit var testRestTemplate: TestRestTemplate
//
//    @Test()
//    fun testApartmentController() {
//        val result = testRestTemplate.getForEntity("/apartments/offers", String::class.java)
//        result `should not be` null
//        result.statusCode `should equal` HttpStatus.OK
//        val apartmentOfJoda = ApartmentOfferDto(
//                price =  BigDecimal.valueOf(1_000_000),
//                location = "Saratov"
//        )
//        val apartmentOfVader = ApartmentOfferDto(
//                price =  BigDecimal.valueOf(3_000_000),
//                location = "Moscow"
//        )
//        val apartments: List<ApartmentOfferDto> = listOf(
//                apartmentOfJoda,
//                apartmentOfVader
//        )
//        result.body `should equal` apartments
//    }
//}