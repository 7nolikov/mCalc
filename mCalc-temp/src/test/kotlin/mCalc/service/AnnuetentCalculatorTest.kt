package mCalc.service


import mCalc.controller.ReportController
import org.amshove.kluent.`should equal`
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.junit4.SpringRunner
import java.math.BigDecimal

@RunWith(SpringRunner::class)
@SpringBootTest
class AnnuetentCalculatorTest {

    @Autowired
    lateinit var reportController: ReportController

    @Test
    fun calculateTest1() {
        val price = BigDecimal.valueOf(7000000)
        val period = 10L
        val interest = 14.0

        val mortgageAgreement = reportController.calculate(price, period, interest)

        mortgageAgreement.userId `should equal` 1
        mortgageAgreement.firstPayment.toLong() `should equal` 1_050_000
        mortgageAgreement.creditSum.toLong() `should equal` 5_950_000
        mortgageAgreement.paymentPerMonth.toLong() `should equal` 92_383
        mortgageAgreement.periodInYears `should equal` 10
        mortgageAgreement.totalPayment.toLong() `should equal` 12_136_023
        mortgageAgreement.overpayment.toLong() `should equal` 5_136_023
    }

    @Test
    fun calculateTest2() {
        val price = BigDecimal.valueOf(2590000)
        val period = 10L
        val interest = 14.0

        val mortgageAgreement = reportController.calculate(price, period, interest)

        mortgageAgreement.userId `should equal` 1
        mortgageAgreement.firstPayment.toLong() `should equal` 388_500
        mortgageAgreement.creditSum.toLong() `should equal` 2_201_500
        mortgageAgreement.paymentPerMonth.toLong() `should equal` 34_181
        mortgageAgreement.periodInYears `should equal` 10
        mortgageAgreement.totalPayment.toLong() `should equal` 4_490_328
        mortgageAgreement.overpayment.toLong() `should equal` 1_900_328
    }
}