package mCalc.exception

import java.lang.RuntimeException

class ApartmentOfferNotFoundException(id: Long) : RuntimeException("Apartment offer with id $id not found")