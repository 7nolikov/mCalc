package mCalc.exception

import java.lang.RuntimeException

class UserAlreadyExistsException(login: String) : RuntimeException("User with login $login already exists")