package mCalc.exception

import java.lang.RuntimeException

class MortgageAgreementNotFoundException(id: Long) : RuntimeException("Mortgage agreement with id $id not found")