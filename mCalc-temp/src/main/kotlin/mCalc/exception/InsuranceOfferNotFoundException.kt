package mCalc.exception

import java.lang.RuntimeException

class InsuranceOfferNotFoundException(id: Long) : RuntimeException("Insurance offer with id $id not found")