package mCalc.exception

import java.lang.RuntimeException

class BankOfferNotFoundException(id: Long) : RuntimeException("Bank offer with id $id not found")