package mCalc.exception

import java.lang.RuntimeException

class UserNotFoundException(login: String) : RuntimeException("User with login $login not found")