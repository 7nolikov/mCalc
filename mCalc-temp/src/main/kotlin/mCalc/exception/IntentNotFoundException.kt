package mCalc.exception

import java.lang.RuntimeException

class IntentNotFoundException(id: Long) : RuntimeException("Intent with id $id not found")