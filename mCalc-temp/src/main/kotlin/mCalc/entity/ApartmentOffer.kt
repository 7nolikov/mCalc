package mCalc.entity

import java.math.BigDecimal
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id

@Entity
class ApartmentOffer(
        @Id @GeneratedValue
        val id: Long,
        val price: BigDecimal,
        val location: String,
        val address: String? = null,
        val square: Long? = null
)
