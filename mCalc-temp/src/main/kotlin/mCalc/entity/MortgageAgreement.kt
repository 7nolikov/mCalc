package mCalc.entity

import java.math.BigDecimal
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id

@Entity
class MortgageAgreement(
    @Id @GeneratedValue
    val id: Long,
    var userId: Long,
    var firstPayment: BigDecimal,
    var creditSum: BigDecimal,
    var periodInYears: Long,
    var paymentPerMonth: BigDecimal,
    var overpayment: BigDecimal,
    var totalPayment: BigDecimal
)
