package mCalc.entity

import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id

@Entity(name = "users")
class User(
        @Id @GeneratedValue
        val id: Long,
        val name: String,
        val login: String,
        val password: String,
        val activated: Boolean = false,
        val email: String,
        var roles: Role = Role.GUEST
)
