package mCalc.entity

import java.math.BigDecimal
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id

@Entity
class InsuranceOffer(
        @Id
        @GeneratedValue
        val id: Long,
        val companyName: String,
        val price: BigDecimal,
        val insurancePayment: BigDecimal
)