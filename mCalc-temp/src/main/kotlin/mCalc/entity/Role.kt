package mCalc.entity

enum class Role {
    GUEST, USER, ADMIN
}