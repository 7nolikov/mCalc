package mCalc.entity

import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id

@Entity
class BankOffer(
        @Id
        @GeneratedValue
        val id: Long,
        val name: String,
        val interestRate: Double
)