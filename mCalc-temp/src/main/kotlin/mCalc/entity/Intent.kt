package mCalc.entity

import java.math.BigDecimal
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id

@Entity
data class Intent(
    @Id @GeneratedValue
    var id: Long,
    val userId: Long,
    val maxPrice: BigDecimal,
    val location: String,
    val periodInYears: Long,
    val interestRate: Double
)
