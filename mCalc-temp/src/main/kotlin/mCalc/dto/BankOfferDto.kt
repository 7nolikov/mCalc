package mCalc.dto

class BankOfferDto(
        val name: String,
        val interestRate: Double
)