package mCalc.dto

class UserDto(
        val name: String,
        val login: String,
        val password: String,
        val email: String
)