package mCalc.dto

import java.math.BigDecimal

class IntentDto(
        val userId: Long,
        val maxPrice: BigDecimal,
        val location: String,
        val periodInYears: Long,
        val interestRate: Double
)
