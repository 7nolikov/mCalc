package mCalc.dto

import java.math.BigDecimal

data class AgreementDto(
    val price: BigDecimal,
    val period: Long,
    val interest: Double
)