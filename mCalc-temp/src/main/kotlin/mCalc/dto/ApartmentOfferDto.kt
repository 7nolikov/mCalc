package mCalc.dto

import java.math.BigDecimal

class ApartmentOfferDto(
        val price: BigDecimal,
        val location: String,
        val address: String? = null,
        val square: Long? = null
)