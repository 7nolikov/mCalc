package mCalc.dto

import java.math.BigDecimal

class InsuranceOfferDto(
        val companyName: String,
        val price: BigDecimal,
        val insurancePayment: BigDecimal
)