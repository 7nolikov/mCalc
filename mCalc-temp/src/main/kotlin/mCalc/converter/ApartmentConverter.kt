package mCalc.converter

import mCalc.dto.ApartmentOfferDto
import mCalc.entity.ApartmentOffer
import org.springframework.core.convert.converter.Converter

class ApartmentConverter: Converter<ApartmentOffer, ApartmentOfferDto> {
    override fun convert(source: ApartmentOffer): ApartmentOfferDto {
        return ApartmentOfferDto(
                price = source.price,
                location = source.location,
                address = source.address,
                square = source.square
        )
    }
}