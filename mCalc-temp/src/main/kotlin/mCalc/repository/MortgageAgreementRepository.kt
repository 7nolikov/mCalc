package mCalc.repository

import mCalc.entity.MortgageAgreement
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface MortgageAgreementRepository : JpaRepository<MortgageAgreement, Long>
