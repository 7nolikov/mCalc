package mCalc.repository

import mCalc.entity.ApartmentOffer
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository


@Repository
interface ApartmentOfferRepository : JpaRepository<ApartmentOffer, Long>
