package mCalc.repository

import mCalc.entity.BankOffer
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository


@Repository
interface BankOfferRepository : JpaRepository<BankOffer, Long>
