package mCalc.repository

import mCalc.entity.InsuranceOffer
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository


@Repository
interface InsuranceOfferRepository : JpaRepository<InsuranceOffer, Long>
