package mCalc.repository

import mCalc.entity.Intent
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface IntentRepository : JpaRepository<Intent, Long>
