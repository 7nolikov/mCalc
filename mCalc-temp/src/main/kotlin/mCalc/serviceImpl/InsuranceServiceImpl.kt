package mCalc.serviceImpl

import mCalc.dto.BankOfferDto
import mCalc.dto.InsuranceOfferDto
import mCalc.service.DataBaseService
import mCalc.service.InsuranceService
import org.springframework.core.convert.ConversionService
import org.springframework.stereotype.Service

@Service
class InsuranceServiceImpl(val dataBaseService: DataBaseService, val conversionService: ConversionService): InsuranceService {
    override fun getAllOffers(): List<InsuranceOfferDto> {
        val insuranceOffers = dataBaseService.findAllInsuranceOffers()
        return insuranceOffers.map { offer -> conversionService.convert(offer, InsuranceOfferDto::class.java)!! }
    }

    override fun getById(id: Long): InsuranceOfferDto {
        return conversionService.convert(dataBaseService.findInsuranceOfferById(id), InsuranceOfferDto::class.java)!!
    }

    override fun fetchInsuranceOffers(offersProviders: List<String>) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}