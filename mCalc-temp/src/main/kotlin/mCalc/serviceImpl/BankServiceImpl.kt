package mCalc.serviceImpl

import mCalc.dto.ApartmentOfferDto
import mCalc.dto.BankOfferDto
import mCalc.service.BankService
import mCalc.service.DataBaseService
import org.springframework.core.convert.ConversionService
import org.springframework.stereotype.Service

@Service
class BankServiceImpl(val dataBaseService: DataBaseService, val conversionService: ConversionService): BankService {
    override fun getAllOffers(): List<BankOfferDto> {
        val bankOffers = dataBaseService.findAllBankOffers()
        return bankOffers.map { offer -> conversionService.convert(offer, BankOfferDto::class.java)!! }
    }

    override fun getById(id: Long): BankOfferDto {
        return conversionService.convert(dataBaseService.findBankOfferById(id), BankOfferDto::class.java)!!
    }

    override fun fetchBankOffers(offersProviders: List<String>) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}