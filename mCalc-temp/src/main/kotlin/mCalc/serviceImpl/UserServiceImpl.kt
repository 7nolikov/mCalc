package mCalc.serviceImpl

import mCalc.dto.UserDto
import mCalc.entity.User
import mCalc.exception.UserAlreadyExistsException
import mCalc.service.DataBaseService
import mCalc.service.UserService
import org.springframework.core.convert.ConversionService
import org.springframework.stereotype.Service

@Service
class UserServiceImpl(val dataBaseService: DataBaseService, val conversionService: ConversionService) : UserService {
    override fun get(login: String): UserDto {
        TODO("not implemented")
    }

    override fun create(userDto: UserDto): UserDto {
        checkIfUserAlreadyExists(userDto.login)
        checkIfPasswordMatchPattern(userDto.password)
        val savedUser = dataBaseService.saveUser(conversionService.convert(userDto, User::class.java)!!)
        return conversionService.convert(savedUser, UserDto::class.java)!!
    }

    private fun checkIfUserAlreadyExists(login: String) {
        if (checkIfUserExist(login)) {
            throw UserAlreadyExistsException(login)
        }
    }

    override fun checkIfUserExist(login: String): Boolean {
        TODO("not implemented")
    }

    private fun checkIfPasswordMatchPattern(password: String) {
        TODO("not implemented")
    }

    override fun update(userDto: UserDto): UserDto {
        val user = conversionService.convert(userDto, User::class.java)!!
        val savedUser = dataBaseService.saveUser(user)
        return conversionService.convert(savedUser, UserDto::class.java)!!
    }

    override fun delete(login: String) {
        dataBaseService.deleteUser(login)
    }
}