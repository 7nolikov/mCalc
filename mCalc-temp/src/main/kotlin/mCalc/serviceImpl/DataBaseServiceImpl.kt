package mCalc.serviceImpl

import mCalc.entity.*
import mCalc.exception.*
import mCalc.repository.*
import mCalc.service.DataBaseService
import org.springframework.stereotype.Service

@Service
class DataBaseServiceImpl(
        val apartmentOfferRepository: ApartmentOfferRepository,
        val bankOfferRepository: BankOfferRepository,
        val insuranceOfferRepository: InsuranceOfferRepository,
        val intentRepository: IntentRepository,
        val mortgageAgreementRepository: MortgageAgreementRepository,
        val userRepository: UserRepository
) : DataBaseService {
    override fun findApartmentOfferById(id: Long): ApartmentOffer {
        return apartmentOfferRepository.findById(id).orElseThrow { ApartmentOfferNotFoundException(id) }
    }

    override fun findAllApartmentOffers(): List<ApartmentOffer> {
        return apartmentOfferRepository.findAll()
    }

    override fun saveApartmentOffers(apartmentOffers: List<ApartmentOffer>): List<ApartmentOffer> {
        return apartmentOfferRepository.saveAll(apartmentOffers)
    }

    override fun findBankOfferById(id: Long): BankOffer {
        return bankOfferRepository.findById(id).orElseThrow { BankOfferNotFoundException(id) }
    }

    override fun findAllBankOffers(): List<BankOffer> {
        return bankOfferRepository.findAll()
    }

    override fun saveBankOffers(bankOffers: List<BankOffer>): List<BankOffer> {
        return bankOfferRepository.saveAll(bankOffers)
    }

    override fun findInsuranceOfferById(id: Long): InsuranceOffer {
        return insuranceOfferRepository.findById(id).orElseThrow { InsuranceOfferNotFoundException(id) }
    }

    override fun findAllInsuranceOffers(): List<InsuranceOffer> {
        return insuranceOfferRepository.findAll()
    }

    override fun saveInsuranceOffers(insuranceOffers: List<InsuranceOffer>): List<InsuranceOffer> {
        return insuranceOfferRepository.saveAll(insuranceOffers)
    }

    override fun findIntentById(id: Long): Intent {
        return intentRepository.findById(id).orElseThrow { IntentNotFoundException(id) }
    }

    override fun findAllIntents(): List<Intent> {
        return intentRepository.findAll()
    }

    override fun saveIntent(intent: Intent): Intent {
        return intentRepository.save(intent)
    }

    override fun deleteIntent(intent: Intent) {
        intentRepository.delete(intent)
    }

    override fun findMortgageAgreementById(id: Long): MortgageAgreement {
        return mortgageAgreementRepository.findById(id).orElseThrow { MortgageAgreementNotFoundException(id) }
    }

    override fun findAllMortgageAgreements(): List<MortgageAgreement> {
        return mortgageAgreementRepository.findAll()
    }

    override fun saveMortgageAgreement(mortgageAgreement: MortgageAgreement): MortgageAgreement {
        return mortgageAgreementRepository.save(mortgageAgreement)
    }

    override fun deleteMortgageAgreement(mortgageAgreement: MortgageAgreement) {
        mortgageAgreementRepository.delete(mortgageAgreement)
    }

    override fun findUserByLogin(login: String): User {
        return userRepository.findByLogin(login).orElseThrow { UserNotFoundException(login) }
    }

    override fun saveUser(user: User): User {
        return userRepository.save(user)
    }

    override fun deleteUser(login: String) {
        userRepository.delete(findUserByLogin(login))
    }
}