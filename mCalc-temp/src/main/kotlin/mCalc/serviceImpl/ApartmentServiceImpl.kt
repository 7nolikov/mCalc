package mCalc.serviceImpl

import mCalc.dto.ApartmentOfferDto
import mCalc.service.ApartmentService
import mCalc.service.DataBaseService
import org.springframework.core.convert.ConversionService
import org.springframework.stereotype.Service

@Service
class ApartmentServiceImpl(val dataBaseService: DataBaseService, val conversionService: ConversionService) : ApartmentService {
    override fun getAllOffers(): List<ApartmentOfferDto> {
        val apartmentOffers = dataBaseService.findAllApartmentOffers()
        return apartmentOffers.map { offer -> conversionService.convert(offer, ApartmentOfferDto::class.java)!! }
    }

    override fun getById(id: Long): ApartmentOfferDto {
        return conversionService.convert(dataBaseService.findApartmentOfferById(id), ApartmentOfferDto::class.java)!!
    }

    override fun fetchApartmentOffers(offersProviders: List<String>) {
        TODO("not implemented")
    }
}