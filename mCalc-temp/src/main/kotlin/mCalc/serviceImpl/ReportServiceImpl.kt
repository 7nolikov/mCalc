package mCalc.serviceImpl

import mCalc.Constants
import mCalc.dto.AgreementDto
import mCalc.entity.Intent
import mCalc.entity.MortgageAgreement
import mCalc.service.DataBaseService
import mCalc.service.ReportService
import org.springframework.core.convert.ConversionService
import org.springframework.stereotype.Service
import java.math.BigDecimal

@Service
class ReportServiceImpl(val dataBaseService: DataBaseService, val conversionService: ConversionService) : ReportService {
    override fun createReport(intent: Intent): MortgageAgreement {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun calculateAgreementParameters(
        agreementDto: AgreementDto
    ): MortgageAgreement {

        val price = agreementDto.price
        val period = agreementDto.period
        val interest = agreementDto.interest

        val interestDecimal = interest / 100
        val interestRatePerMonth = interestDecimal / 12
        val periodInMonths = period * 12.0
        val x = Math.pow(1 + interestRatePerMonth, periodInMonths)
        val y = x / (x - 1)

        val creditSum = BigDecimal.valueOf(price.toDouble() * Constants.CREDIT_RATE)
        val firstPayment = BigDecimal.valueOf(price.toDouble() - creditSum.toDouble())
        val paymentPerMonth = BigDecimal.valueOf(interestRatePerMonth * y * creditSum.toDouble())
        val totalPayment = paymentPerMonth * BigDecimal.valueOf(periodInMonths) + firstPayment
        val overpayment = totalPayment - price

        return MortgageAgreement(
            id = 1L,
            userId = 1L,
            firstPayment = firstPayment,
            creditSum = creditSum,
            paymentPerMonth = paymentPerMonth,
            periodInYears = period,
            totalPayment = totalPayment,
            overpayment = overpayment
        )
    }
}