package mCalc.serviceImpl

import mCalc.dto.IntentDto
import mCalc.entity.Intent
import mCalc.entity.User
import mCalc.service.DataBaseService
import mCalc.service.IntentService
import org.springframework.core.convert.ConversionService
import org.springframework.stereotype.Service

@Service
class IntentServiceImpl(val dataBaseService: DataBaseService, val conversionService: ConversionService) : IntentService {
    override fun getAllIntents(user: User): List<IntentDto> {
        val intents = dataBaseService.findAllIntents()
        return intents.map { intent -> conversionService.convert(intent, IntentDto::class.java)!! }
    }

    override fun getById(id: Long): IntentDto {
        return conversionService.convert(dataBaseService.findIntentById(id), IntentDto::class.java)!!
    }

    override fun create(intentDto: IntentDto): IntentDto {
        val intent = conversionService.convert(intentDto, Intent::class.java)!!
        val savedIntent = dataBaseService.saveIntent(intent)
        return conversionService.convert(savedIntent, IntentDto::class.java)!!
    }

    override fun update(id: Long, intentDto: IntentDto): IntentDto {
        val intent = conversionService.convert(intentDto, Intent::class.java)!!
        intent.id = id
        val savedIntent = dataBaseService.saveIntent(intent)
        return conversionService.convert(savedIntent, IntentDto::class.java)!!
    }

    override fun delete(id: Long): IntentDto {
        val intent = dataBaseService.findIntentById(id)
        val deletedIntent = dataBaseService.deleteIntent(intent)
        return conversionService.convert(deletedIntent, IntentDto::class.java)!!
    }
}