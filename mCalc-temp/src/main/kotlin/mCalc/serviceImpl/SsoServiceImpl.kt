package mCalc.serviceImpl

import mCalc.dto.UserDto
import mCalc.exception.UserAlreadyExistsException
import mCalc.service.SsoService
import mCalc.service.UserService
import org.springframework.stereotype.Service

@Service
class SsoServiceImpl(val userService: UserService) : SsoService {
    override fun register(userDto: UserDto) {
        userService.create(userDto)
    }

    override fun loginViaGoogle() {
        TODO("not implemented")
    }

    override fun loginViaVk() {
        TODO("not implemented")
    }

    override fun login(login: String, password: String): String {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun logout(login: String) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun resetPassword(login: String) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}