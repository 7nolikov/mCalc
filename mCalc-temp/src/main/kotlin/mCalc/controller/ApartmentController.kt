package mCalc.controller

import mCalc.dto.ApartmentOfferDto
import mCalc.service.ApartmentService
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.GetMapping

@Controller
class ApartmentController(private val apartmentService: ApartmentService) {

    @GetMapping("/apartments/offers")
    fun getApartmentOffers(): List<ApartmentOfferDto> {
        return apartmentService.getAllOffers()
    }
}