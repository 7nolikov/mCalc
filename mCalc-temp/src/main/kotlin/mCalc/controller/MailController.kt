package mCalc.controller

import mCalc.entity.Intent
import mCalc.service.MailService
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody

@Controller
class MailController(private val mailService: MailService) {

    @GetMapping("/notifications/best")
    fun sendBestOfferNotifications() {
        mailService.sendBestOfferNotifications()
    }

    @GetMapping("/notifications/verification/{login}")
    fun sendVerificationEmail(@PathVariable login: String) {
        mailService.sendVerificationEmail(login)
    }

    @PostMapping("/notifications/subscribe/{login}")
    fun subscribe(@PathVariable login: String, @RequestBody intent: Intent) {
        mailService.subscribe(login, intent)
    }

    @GetMapping("/notifications/unsubscribe/{login}")
    fun unsibscribe(@PathVariable login: String) {
        mailService.unsubscribe(login)
    }
}