package mCalc.controller

import mCalc.dto.AgreementDto
import mCalc.entity.Intent
import mCalc.entity.MortgageAgreement
import mCalc.service.ReportService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestParam
import java.math.BigDecimal

@Controller
class ReportController {

    @Autowired
    lateinit var reportService: ReportService

    @GetMapping("/calculate")
    fun calculate(
            @RequestParam(value = "price") price: BigDecimal,
            @RequestParam(value = "period") period: Long,
            @RequestParam(value = "interest") interest: Double
    ): MortgageAgreement {
        return reportService.calculateAgreementParameters(
                AgreementDto(
                        price = price,
                        period = period,
                        interest = interest
                )
        )
    }

    @PostMapping("/report")
    fun createReport(@RequestBody intent: Intent): MortgageAgreement {
        return reportService.createReport(intent)
    }
}