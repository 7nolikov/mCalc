package mCalc.controller

import mCalc.dto.InsuranceOfferDto
import mCalc.service.InsuranceService
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.GetMapping

@Controller
class InsuranceController(private val insuranceService: InsuranceService) {

    @GetMapping("/insurance/offers")
    fun getInsuranceOffers(): List<InsuranceOfferDto> {
        return insuranceService.getAllOffers()
    }
}