package mCalc.controller

import mCalc.dto.UserDto
import mCalc.service.UserService
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.*

@Controller
class UserController(private val userService: UserService) {

    @GetMapping("/users/{login}")
    fun getUser(@PathVariable login: String): UserDto {
        return userService.get(login)
    }

    @PutMapping("/users/{login}")
    fun updateUser(@PathVariable login: String, @RequestBody userDto: UserDto): UserDto {
        return userService.update(userDto)
    }

    @DeleteMapping("/users/{login}")
    fun deleteUser(@PathVariable login: String) {
        userService.delete(login)
    }
}