package mCalc.controller

import mCalc.dto.UserDto
import mCalc.entity.User
import mCalc.service.SsoService
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestParam

@Controller
class SsoController(private val ssoService: SsoService) {

    @GetMapping("/auth/register")
    fun register(@RequestBody userDto: UserDto) {
        ssoService.register(userDto)
    }

    @GetMapping("/auth/google")
    fun registerViaGoogle(user: User) {
        ssoService.loginViaGoogle()
    }

    @GetMapping("/auth/vk")
    fun registerViaVk(user: User) {
        ssoService.loginViaVk()
    }

    @GetMapping("/auth/login")
    fun login(@RequestParam login: String, @RequestParam password: String): String {
        return ssoService.login(login, password)
    }

    @GetMapping("/auth/logout/{login}")
    fun logout(@PathVariable login: String) {
        ssoService.logout(login)
    }

    @GetMapping("/auth/reset/{login}")
    fun resetPassword(@PathVariable login: String) {
        ssoService.resetPassword(login)
    }
}