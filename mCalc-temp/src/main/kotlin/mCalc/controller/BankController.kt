package mCalc.controller

import mCalc.dto.BankOfferDto
import mCalc.entity.ApartmentOffer
import mCalc.service.BankService
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.GetMapping

@Controller
class BankController(private val bankService: BankService) {

    @GetMapping("/banks/offers")
    fun getBankOffers(): List<BankOfferDto> {
        return bankService.getAllOffers()
    }
}