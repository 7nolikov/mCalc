package mCalc.controller

import mCalc.service.PermissionService
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestParam

@Controller
class PermissionController(private val permissionService: PermissionService) {

    @GetMapping("/permissions")
    fun checkPermissions(@RequestParam login: String, @RequestParam target: String): Boolean{
        return permissionService.checkPermission(login, target)
    }
}