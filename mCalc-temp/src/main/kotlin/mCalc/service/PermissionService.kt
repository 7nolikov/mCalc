package mCalc.service

interface PermissionService {
    fun checkPermission(login: String, target: String): Boolean
}