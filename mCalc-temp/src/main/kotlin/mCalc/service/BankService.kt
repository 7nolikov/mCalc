package mCalc.service

import mCalc.dto.BankOfferDto

interface BankService {
    fun getAllOffers(): List<BankOfferDto>
    fun getById(id: Long): BankOfferDto
    fun fetchBankOffers(offersProviders: List<String>)
}