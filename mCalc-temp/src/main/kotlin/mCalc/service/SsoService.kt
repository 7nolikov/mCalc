package mCalc.service

import mCalc.dto.UserDto

interface SsoService {
    fun register(userDto: UserDto)
    fun loginViaGoogle()
    fun loginViaVk()
    fun login(login: String, password: String) : String
    fun logout(login: String)
    fun resetPassword(login: String)
}