package mCalc.service

import mCalc.dto.AgreementDto
import mCalc.entity.Intent
import mCalc.entity.MortgageAgreement

interface ReportService {
    fun calculateAgreementParameters(agreementDto: AgreementDto): MortgageAgreement
    fun createReport(intent: Intent): MortgageAgreement
}