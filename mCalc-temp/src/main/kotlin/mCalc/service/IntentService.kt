package mCalc.service

import mCalc.dto.IntentDto
import mCalc.entity.User

interface IntentService {
    fun getAllIntents(user: User): List<IntentDto>
    fun getById(id: Long): IntentDto
    fun create(intentDto: IntentDto): IntentDto
    fun update(id: Long, intentDto: IntentDto): IntentDto
    fun delete(id: Long): IntentDto
}