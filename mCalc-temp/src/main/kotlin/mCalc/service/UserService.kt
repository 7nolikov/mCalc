package mCalc.service

import mCalc.dto.UserDto

interface UserService {
    fun get(login: String): UserDto
    fun create(userDto: UserDto): UserDto
    fun update(userDto: UserDto): UserDto
    fun delete(login: String)
    fun checkIfUserExist(login: String): Boolean
}
