package mCalc.service

import mCalc.dto.ApartmentOfferDto

interface ApartmentService {
    fun getAllOffers(): List<ApartmentOfferDto>
    fun getById(id: Long): ApartmentOfferDto
    fun fetchApartmentOffers(offersProviders: List<String>)
}