package mCalc.service

import mCalc.dto.InsuranceOfferDto

interface InsuranceService {
    fun getAllOffers(): List<InsuranceOfferDto>
    fun getById(id: Long): InsuranceOfferDto
    fun fetchInsuranceOffers(offersProviders: List<String>)
}