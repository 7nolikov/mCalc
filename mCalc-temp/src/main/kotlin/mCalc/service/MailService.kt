package mCalc.service

import mCalc.entity.Intent

interface MailService {
    fun sendBestOfferNotifications()
    fun sendVerificationEmail(login: String)
    fun subscribe(login: String, intent: Intent)
    fun unsubscribe(login: String)
}