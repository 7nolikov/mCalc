package mCalc.service

import mCalc.entity.*

interface DataBaseService {
    fun findApartmentOfferById(id: Long): ApartmentOffer
    fun findAllApartmentOffers(): List<ApartmentOffer>
    fun saveApartmentOffers(apartmentOffers: List<ApartmentOffer>): List<ApartmentOffer>
    fun findBankOfferById(id: Long): BankOffer
    fun findAllBankOffers(): List<BankOffer>
    fun saveBankOffers(bankOffers: List<BankOffer>): List<BankOffer>
    fun findInsuranceOfferById(id: Long): InsuranceOffer
    fun findAllInsuranceOffers(): List<InsuranceOffer>
    fun saveInsuranceOffers(insuranceOffers: List<InsuranceOffer>): List<InsuranceOffer>
    fun findIntentById(id: Long): Intent
    fun findAllIntents(): List<Intent>
    fun saveIntent(intent: Intent): Intent
    fun deleteIntent(intent: Intent)
    fun findMortgageAgreementById(id: Long): MortgageAgreement
    fun findAllMortgageAgreements(): List<MortgageAgreement>
    fun saveMortgageAgreement(mortgageAgreement: MortgageAgreement): MortgageAgreement
    fun deleteMortgageAgreement(mortgageAgreement: MortgageAgreement)
    fun findUserByLogin(login: String): User
    fun saveUser(user: User): User
    fun deleteUser(login: String)
}