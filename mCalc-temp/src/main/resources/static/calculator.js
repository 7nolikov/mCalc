$(document).ready(() => {
    fetch(`http://localhost:8080/calculate?${window.location.href.split('?')[1]}`)
        .then(res => res.json())
        .then(data => {
            $('#fpayment').html(data.firstPayment.toFixed(2));
            $('#csum').html(data.creditSum.toFixed(2));
            $('#periodinyears').html(data.periodInYears.toFixed(0));
            $('#paymentpm').html(data.paymentPerMonth.toFixed(2));
            $('#overpayment').html(data.overpayment.toFixed(2));
            $('#totalpayment').html(data.totalPayment.toFixed(2));
        });
});