package mCalc

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class MCalc

fun main(args: Array<String>) {
    runApplication<MCalc>(*args)
}