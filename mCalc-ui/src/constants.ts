import {environment} from './environments/environment'

export const ENDPOINTS = {
  GET_APARTMENT_OFFERS: `${environment.appUrl}/apartments/offers`,
  GET_BANK_OFFERS: `${environment.appUrl}/banks/offers`,
  GET_INSURANCE_OFFERS: `${environment.appUrl}/insurance/offers`,
  GET_NOTIFICATIONS_BEST: `${environment.appUrl}/notifications/best`,
  GET_NOTIFICATIONS_VERIFICATION: `${environment.appUrl}/notifications/verification/{login}`,
  NOTIFICATIONS_SUBSCRIBE: `${environment.appUrl}/notifications/subscribe/{login}`,
  NOTIFICATIONS_UNSUBSCRIBE: `${environment.appUrl}/notifications/unsubscribe/{login}`,
  GET_PERMISSIONS: `${environment.appUrl}/permissions`,
  CALCULATE: `${environment.appUrl}/calculate`,
  GET_REPORT: `${environment.appUrl}/report`,
  REGISTER: `${environment.appUrl}/auth/register`,
  GOOGLE: `${environment.appUrl}/auth/google`,
  VK: `${environment.appUrl}/auth/vk`,
  LOGIN: `${environment.appUrl}/auth/login`,
  LOGOUT: `${environment.appUrl}/auth/logout/{login}`,
  RESET_PASSWORD: `${environment.appUrl}/auth/reset/{login}`,
  USERS: `${environment.appUrl}/users/{login}`,
}