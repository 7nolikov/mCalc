import { Injectable } from '@angular/core';
import { User } from '../model/user';
import { USERS } from '../mock/mock-users';
import { Observable, of } from 'rxjs';
import { MessageService } from './message.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';
import { ENDPOINTS } from 'src/constants';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private userUrl = 'users';

  constructor(
    private http: HttpClient,
    private messageService: MessageService
  ) { }

  getUsers(): Observable<User[]> {
    this.log('fetched users');
    return this.http.get<User[]>(ENDPOINTS.USERS);
  }

  private log(message: string) {
    this.messageService.add(`UserService: ${message}`);
  }

  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(error); // log to console instead
      this.log(`${operation} failed: ${error.message}`);
      return of(result as T);
    };
  }
}
