import { Injectable } from '@angular/core';
import { User } from '../model/user'
import { InMemoryDbService} from 'angular-in-memory-web-api'

@Injectable({
  providedIn: 'root'
})
export class InMemoryDataService implements InMemoryDbService {
  createDb() {
      const users = [
          {
            id: 1,
            name: 'Mr. Nice',
            login: 'login',
            password: 'password',
            email: "email"
          },
          {
            id: 2,
            name: 'Marco',
            login: 'login',
            password: 'password',
            email: "email"
          },
          {
            id: 3,
            name: 'Bombasto',
            login: 'login',
            password: 'password',
            email: "email"
          },
          {
            id: 4,
            name: 'Celeritas',
            login: 'login',
            password: 'password',
            email: "email"
          },
          {
            id: 5,
            name: 'Magneta',
            login: 'login',
            password: 'password',
            email: "email"
          },
          {
            id: 6,
            name: 'RubberMan',
            login: 'login',
            password: 'password',
            email:
            "email"
          },
          {
            id: 7,
            name: 'Dynama',
            login: 'login',
            password: 'password',
            email: "email"
          },
          {
            id: 8,
            name: 'Dr IQ',
            login: 'login',
            password: 'password',
            email: "email"
          },
          {
            id: 9,
            name: 'Magma',
            login: 'login',
            password: 'password',
            email: "email"
          },
          {
            id: 10,
            name: 'Tornado',
            login: 'login',
            password: 'password',
            email: "email"
          }
      ];
      return {users};
   }

   genId(users: User[]): number {
       return users.length > 0 ? Math.max(...users.map(user => user.id)) + 1 : 1;
   }
}
