import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InsurancesPageComponent } from './insurances-page.component';

describe('InsurancesPageComponent', () => {
  let component: InsurancesPageComponent;
  let fixture: ComponentFixture<InsurancesPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InsurancesPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InsurancesPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
