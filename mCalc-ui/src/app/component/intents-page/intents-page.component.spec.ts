import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IntentsPageComponent } from './intents-page.component';

describe('IntentsPageComponent', () => {
  let component: IntentsPageComponent;
  let fixture: ComponentFixture<IntentsPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IntentsPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IntentsPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
