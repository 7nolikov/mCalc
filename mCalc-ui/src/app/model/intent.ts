export class Intent {
  maxPrice: number;
  location: string;
  periodInYears: number;
  interestRate: number;
}
