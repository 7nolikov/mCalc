export class InsuranceOffer {
  companyName: string;
  price: number;
  insurancePayment: number;
}
