export class ApartmentOffer {
  price: number;
  location: string;
  address: string;
  square: number;
}
