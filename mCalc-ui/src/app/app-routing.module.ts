import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginPageComponent } from "./component/login-page/login-page.component";
import { LoginFormComponent } from "./component/login-form/login-form.component";
import { LogoutPageComponent } from "./component/logout-page/logout-page.component";
import { ApartmentsPageComponent } from "./component/apartments-page/apartments-page.component";
import { BanksPageComponent } from "./component/banks-page/banks-page.component";
import { InsurancesPageComponent } from "./component/insurances-page/insurances-page.component";
import { UsersPageComponent } from "./component/users-page/users-page.component";
import { IntentsPageComponent } from "./component/intents-page/intents-page.component";
import { NotificationsPageComponent } from "./component/notifications-page/notifications-page.component";
import { ReportsPageComponent } from "./component/reports-page/reports-page.component";
import { MainPageComponent } from "./component/main-page/main-page.component";
import { RegistrationFormComponent } from "./component/registration-form/registration-form.component";

const routes: Routes = [
  { path: 'login', component: LoginPageComponent },
  { path: 'login-form', component: LoginFormComponent },
  { path: 'logout', component: LogoutPageComponent },
  { path: 'register', component: RegistrationFormComponent },
  { path: 'apartments', component: ApartmentsPageComponent },
  { path: 'banks', component: BanksPageComponent },
  { path: 'insurances', component: InsurancesPageComponent },
  { path: 'users', component: UsersPageComponent },
  { path: 'intents', component: IntentsPageComponent },
  { path: 'notifications', component: NotificationsPageComponent },
  { path: 'reports', component: ReportsPageComponent },
  { path: '', component: MainPageComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
