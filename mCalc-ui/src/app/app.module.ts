import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule }    from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavigationBarComponent } from './component/navigation-bar/navigation-bar.component';
import { LoginPageComponent } from './component/login-page/login-page.component';
import { MainPageComponent } from './component/main-page/main-page.component';
import { ApartmentsPageComponent } from './component/apartments-page/apartments-page.component';
import { BanksPageComponent } from './component/banks-page/banks-page.component';
import { InsurancesPageComponent } from './component/insurances-page/insurances-page.component';
import { UsersPageComponent } from './component/users-page/users-page.component';
import { IntentsPageComponent } from './component/intents-page/intents-page.component';
import { NotificationsPageComponent } from './component/notifications-page/notifications-page.component';
import { ReportsPageComponent } from './component/reports-page/reports-page.component';
import { LogoutPageComponent } from './component/logout-page/logout-page.component';
import { RegistrationFormComponent } from './component/registration-form/registration-form.component';
import { LoginFormComponent } from './component/login-form/login-form.component';
import { MessagesComponent } from './component/messages/messages.component';

import { HttpClientInMemoryWebApiModule } from 'angular-in-memory-web-api';
import { InMemoryDataService }  from './service/in-memory-data.service';

@NgModule({
  declarations: [
    AppComponent,
    NavigationBarComponent,
    LoginPageComponent,
    RegistrationFormComponent,
    MainPageComponent,
    ApartmentsPageComponent,
    BanksPageComponent,
    InsurancesPageComponent,
    UsersPageComponent,
    IntentsPageComponent,
    NotificationsPageComponent,
    ReportsPageComponent,
    LogoutPageComponent,
    LoginFormComponent,
    MessagesComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,

    HttpClientModule,

    // The HttpClientInMemoryWebApiModule module intercepts HTTP requests
    // and returns simulated server responses.
    // Remove it when a real server is ready to receive requests.
    HttpClientInMemoryWebApiModule.forRoot(
      InMemoryDataService, { dataEncapsulation: false }
    )
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
