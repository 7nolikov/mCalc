import { User } from '../model/user';

export const USERS: User[] = [
  {
    id: 1,
    name: 'Mr. Nice',
    login: 'login',
    password: 'password',
    email: "email"
  },
  {
    id: 2,
    name: 'Marco',
    login: 'login',
    password: 'password',
    email: "email"
  },
  {
    id: 3,
    name: 'Bombasto',
    login: 'login',
    password: 'password',
    email: "email"
  },
  {
    id: 4,
    name: 'Celeritas',
    login: 'login',
    password: 'password',
    email: "email"
  },
  {
    id: 5,
    name: 'Magneta',
    login: 'login',
    password: 'password',
    email: "email"
  },
  {
    id: 6,
    name: 'RubberMan',
    login: 'login',
    password: 'password',
    email:
    "email"
  },
  {
    id: 7,
    name: 'Dynama',
    login: 'login',
    password: 'password',
    email: "email"
  },
  {
    id: 8,
    name: 'Dr IQ',
    login: 'login',
    password: 'password',
    email: "email"
  },
  {
    id: 9,
    name: 'Magma',
    login: 'login',
    password: 'password',
    email: "email"
  },
  {
    id: 10,
    name: 'Tornado',
    login: 'login',
    password: 'password',
    email: "email"
  }
];
