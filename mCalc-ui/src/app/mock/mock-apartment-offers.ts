import { ApartmentOffer } from '../model/apartment-offer';

export const APARTMENT_OFFERS: ApartmentOffer[] = [
  {
    price: 101,
    location: "Saratov",
    address: "Kirova 11",
    square: 30
  },
  {
    price: 102,
    location: "Saratov",
    address: "Kirova 12",
    square: 34
  },
  {
    price: 103,
    location: "Saratov",
    address: "Kirova 13",
    square: 21
  },
  {
    price: 104,
    location: "Saratov",
    address: "Kirova 14",
    square: 14
  },
  {
    price: 105,
    location: "Saratov",
    address: "Kirova 15",
    square: 44
  },
];
